<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use App\File;

class SiteController extends Controller
{

    public function login()
    {

        //returns view for logging into platform
        return view('auth.login');
    }


    public function index()
    {
        //displays all files by specific username only
        $files = File::where('username', Auth::user()->name)->get();

        return view('pages.index', compact('files'));
    }

    public function download()
    {

        //download variable - UNUSED CODE TO BE ADAPTED
        $download = File::where(['cover_image', 'audio'])->get();
        return view('pages.index', compact('download'));
    }

    public function error()
    {
        //returns an error page
        return view('pages.error');
    }

    public function upload()
    {

        //pulls user data forward for autofill in form
        $user = DB::table('users');

        return view('pages.upload', compact($user));
    }

    public function store(Request $request)
    {

        //validate what is being uploaded for security purposes
        $this->validate($request, [
            'title'=>'required',
            'username' => 'required',
            'cover_image' => 'image|nullable',
            'audio' => 'required'
        ]);

        // Handle File Upload for image
        if($request->hasFile('cover_image')) {
            // Get filename with extension
           $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('/cover_images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        //handles file upload for audio, works same as image
        if($request->hasFile('audio')) {
            // Get filename with extension
            $audionameWithExt = $request->file('audio')->getClientOriginalName();
            // Get just filename
            $audioname = pathinfo($audionameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('audio')->getClientOriginalExtension();
            //Filename to store
            $audioNameToStore = $audioname.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('audio')->storeAs('/audio', $audioNameToStore);
        }



        // creates file post and redirects to homepage after upload
        $post = new File;
        $post->title = $request->input('title');
        $post->username = Auth::user()->name;
        $post->cover_image = $fileNameToStore;
        $post->audio = $audioNameToStore;
        $post->save();
        return redirect('/home')->with('success', 'File Uploaded');
    }
}
