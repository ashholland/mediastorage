@extends('templates.main')

@section('css')
<style>
     .site-container {
         text-align: center;
     }
    .file-container {
        margin: 10px;
        padding: 10px;
    }
</style>
@endsection

@section('content')
    <style>
        .file-container {
            margin: 10px;
            padding: 10px;
            width: 25%;
            float: left;
            position: relative;
        }

        img {
            max-height: 100px;
        }
    </style>

    <div class="home-storage">

        <div class="site-container centre-text">
            <h1 style="text-align: center">Your Files</h1>

            @if(count($files) > 0)
                @foreach($files as $file)

                    <div class="file-container">
                        <img src="{{asset('/storage/cover_images/'.$file->cover_image)}}">
                        <p>{{ $file->title }}</p>
                        <a href ="{{ asset('/storage/audio/'.$file->audio)}}" download="{{ asset('/storage/audio/'.$file->audio)}}" class="button">Download Audio</a>
                        <a href ="{{ asset('/storage/cover_images/'.$file->cover_image)}}" download="{{ asset('/storage/cover_images/'.$file->cover_image)}}" class="button">Download Cover Image</a>
                        <br>
                        <audio controls>
                            <source src="{{ asset('/storage/audio/'.$file->audio)}}" type="audio/mp3">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                @endforeach
            @else

            @endif

        </div>
    </div>


@endsection
