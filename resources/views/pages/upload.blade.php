@extends('templates.main')


@section('content')

    <style>
        .site-container {
            padding: 10px;
            margin: 10px;
        }
    </style>

    <div class="upload-section">

        <div class="site-container">
            <h1>Upload File</h1>

            {!! Form::open(['action' => 'SiteController@store', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
            {{ csrf_field() }}
            <div class="form-group">
                {{Form::label('title')}}
                {{Form::text('title')}}
                <br>
                {{Form::label('username')}}
                {{Form::text('username', Auth::user()->name)}}
                <br>
                {{Form::label('cover image')}}
                {{Form::file('cover_image')}}
                <br>
                {{Form::label('audio file')}}
                {{Form::file('audio')}}
                <br>
                {{Form::submit('Submit')}}
            </div>

            {!! Form::close() !!}

        </div>

    </div>

@endsection
