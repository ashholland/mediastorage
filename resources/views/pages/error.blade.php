<div class="error-page">
    <div class="site-container" style="text-align: center">
        <h1>Whoops!</h1>

        <h2>Looks like you've found an error</h2>
        <p>Don't worry! Our highly trained dev team are working hard to fix this problem!</p>

        <div class="error-gif">
        <img src="/images/errorpusheen.gif">
        </div>
    </div>
</div>
