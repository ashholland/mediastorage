<?php


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@login');
Route::get('/register', 'SiteController@register');
Route::get('/forgotten-password', 'SiteController@forgotPassword');
Route::get('/error', 'SiteController@error');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'SiteController@index');
    Route::post('/home', 'SiteController@store');
    Route::get('/upload', 'SiteController@upload');
    Route::post('/upload', 'SiteController@store');
});


